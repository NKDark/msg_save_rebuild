package com.nkdark.repository;

import com.nkdark.entity.GroupMemberEntity;
import org.springframework.data.repository.CrudRepository;

public interface GroupMemberRepository extends CrudRepository<GroupMemberEntity, Long> {
}
