package com.nkdark.repository;

import com.nkdark.entity.FriendEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRepository extends CrudRepository<FriendEntity, Long> {
}
