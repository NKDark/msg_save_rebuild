package com.nkdark.repository;

import com.nkdark.entity.PrivateMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrivateMessageRepository extends JpaRepository<PrivateMessageEntity, Long> {
}
