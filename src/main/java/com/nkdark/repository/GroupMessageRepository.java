package com.nkdark.repository;

import com.nkdark.entity.GroupMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupMessageRepository extends JpaRepository<GroupMessageEntity, Long> {
}
