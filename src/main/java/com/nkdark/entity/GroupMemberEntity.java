package com.nkdark.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "group_member_info")
public class GroupMemberEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Long groupId;

    private Long userId;

    private String nickname;

    private String card;

    private String sex;

    private Integer age;

    private String area;

    private Long joinTime;

    private Long lastSentTime;

    private String level;

    private String role;

    private boolean unfriendly;

    private String title;

    private Long titleExpireTime;

    private boolean cardChangeable;

}
