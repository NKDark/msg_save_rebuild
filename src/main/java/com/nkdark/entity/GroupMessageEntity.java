package com.nkdark.entity;

import lombok.Data;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "group_message")
public class GroupMessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Long groupId;

    private Long userId;

    private Long time;

    @Lob
    @Column(columnDefinition = "text")
    private String rawMessage;

    private Date eventTime;

    private Integer messageId;

}
