package com.nkdark.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "friend_info")
public class FriendEntity {

    @Id
    private Long userId;

    private String nickname;

    private String remark;
}
