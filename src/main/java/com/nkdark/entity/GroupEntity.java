package com.nkdark.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "group_info")
public class GroupEntity {

    @Id
    private Long groupId;

    private String groupName;

    private Integer memberCount;

    private Integer maxMemberCount;
}
