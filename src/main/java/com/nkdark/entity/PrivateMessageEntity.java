package com.nkdark.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "private_message")
public class PrivateMessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Long userId;

    private Long time;

    @Lob
    @Column(columnDefinition = "text")
    private String rawMessage;

    private Date eventTime;

    private Integer messageId;
}
