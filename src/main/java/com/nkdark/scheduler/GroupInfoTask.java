package com.nkdark.scheduler;

import com.nkdark.entity.GroupMemberEntity;
import com.nkdark.repository.GroupMemberRepository;
import net.lz1998.pbbot.bot.BotContainer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

@Component
public class GroupInfoTask {

    @Resource
    private BotContainer botContainer;

    @Resource
    private GroupMemberRepository groupMemberRepository;

    @Scheduled(cron = "30 10 0 * * *")
    public void getGroupList() {
        try {
            botContainer.getBots().forEach((k, v) ->
                    Objects.requireNonNull(v.getGroupList()).getGroupList().forEach(it ->
                            Objects.requireNonNull(v.getGroupMemberList(it.getGroupId()))
                                    .getGroupMemberList().forEach(item -> {
                                        GroupMemberEntity groupMember = new GroupMemberEntity();
                                        groupMember.setGroupId(item.getGroupId());
                                        groupMember.setUserId(item.getUserId());
                                        groupMember.setNickname(item.getNickname());
                                        groupMember.setCard(item.getCard());
                                        groupMember.setSex(item.getSex());
                                        groupMember.setAge(item.getAge());
                                        groupMember.setArea(item.getArea());
                                        groupMember.setJoinTime(item.getJoinTime());
                                        groupMember.setLastSentTime(item.getLastSentTime());
                                        groupMember.setLevel(item.getLevel());
                                        groupMember.setRole(item.getRole());
                                        groupMember.setUnfriendly(item.getUnfriendly());
                                        groupMember.setTitle(item.getTitle());
                                        groupMember.setTitleExpireTime(item.getTitleExpireTime());
                                        groupMember.setCardChangeable(item.getCardChangeable());
                                        groupMemberRepository.save(groupMember);
                                    })
                    )
            );
        } catch (Exception ignored) {}
    }

}
