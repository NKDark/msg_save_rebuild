package com.nkdark.scheduler;

import com.nkdark.entity.FriendEntity;
import com.nkdark.repository.FriendRepository;
import net.lz1998.pbbot.bot.BotContainer;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

@Component
public class FriendInfoTask {

    @Resource
    private BotContainer botContainer;

    @Resource
    private FriendRepository friendRepository;

    @Scheduled(cron = "10 30 0 * * *")
    public void getFriendList() {
        try {
            botContainer.getBots().forEach((k, v) ->
                    Objects.requireNonNull(v.getFriendList()).getFriendList().forEach(item -> {
                        FriendEntity friend = new FriendEntity();
                        friend.setNickname(item.getNickname());
                        friend.setUserId(item.getUserId());
                        friend.setRemark(item.getRemark());
                        friendRepository.save(friend);
                    })
            );
        } catch (Exception ignored) {}
    }
}
