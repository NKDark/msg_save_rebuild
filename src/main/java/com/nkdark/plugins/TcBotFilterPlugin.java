package com.nkdark.plugins;

import lombok.extern.slf4j.Slf4j;
import net.lz1998.pbbot.bot.Bot;
import net.lz1998.pbbot.bot.BotPlugin;
import onebot.OnebotEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TcBotFilterPlugin extends BotPlugin {
    @Override
    public int onGroupMessage(@NotNull Bot bot, @NotNull OnebotEvent.GroupMessageEvent event) {
        long userId = event.getUserId();
        long startId = 2854196300L;
        long endId = 2854216399L;
        if (userId >= startId && userId <= endId) {
            log.info("已拦截QQ官方机器人消息，ID：[{}]", userId);
            return MESSAGE_BLOCK;
        }
        return MESSAGE_IGNORE;
    }
}
