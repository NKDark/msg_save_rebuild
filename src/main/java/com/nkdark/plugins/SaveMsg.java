package com.nkdark.plugins;

import com.nkdark.entity.GroupMessageEntity;
import com.nkdark.entity.PrivateMessageEntity;
import com.nkdark.repository.GroupMessageRepository;
import com.nkdark.repository.PrivateMessageRepository;
import net.lz1998.pbbot.bot.Bot;
import net.lz1998.pbbot.bot.BotPlugin;
import onebot.OnebotEvent;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
public class SaveMsg extends BotPlugin {

    @Resource
    private GroupMessageRepository groupMessageRepository;

    @Resource
    private PrivateMessageRepository privateMessageRepository;

    @Override
    public int onGroupMessage(@NotNull Bot bot, @NotNull OnebotEvent.GroupMessageEvent event) {
        GroupMessageEntity message = new GroupMessageEntity();
        message.setGroupId(event.getGroupId());
        message.setUserId(event.getUserId());
        message.setTime(event.getTime());
        message.setRawMessage(event.getRawMessage().replace("\n", "\\\\N"));
        message.setEventTime(new Date((event.getTime() + 28800) * 1000));
        message.setMessageId(event.getMessageId());
        groupMessageRepository.save(message);
        return super.onGroupMessage(bot, event);
    }

    @Override
    public int onPrivateMessage(@NotNull Bot bot, @NotNull OnebotEvent.PrivateMessageEvent event) {
        PrivateMessageEntity message = new PrivateMessageEntity();
        message.setUserId(event.getUserId());
        message.setTime(event.getTime());
        message.setRawMessage(event.getRawMessage().replace("\n", "\\\\N"));
        message.setEventTime(new Date((event.getTime() + 28800) * 1000));
        message.setMessageId(event.getMessageId());
        privateMessageRepository.save(message);
        return super.onPrivateMessage(bot, event);
    }
}
