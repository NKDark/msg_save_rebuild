package com.nkdark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MsgSaveRebuildApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsgSaveRebuildApplication.class, args);
    }

}
